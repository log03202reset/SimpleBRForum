<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SimpleBR Forum</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>
    <div id="error">

        <?php
        $reason = isset($_GET['reason']) ? $_GET['reason'] : "Unspecified reason";

// Displays the reason for the ban to the user
        echo "You were banned for: $reason";
        ?>
        <br>

        <a href="index.php" class="homepage_link">Home Page</a>
    </div>
</body>
</html>
